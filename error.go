/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package sparks

import "errors"

var (
	// ErrUserPoolNotFound is returned when a user pool does not exist
	ErrUserPoolNotFound = errors.New("user pool not found")

	// ErrUserNotFound is returned when a user does not exist
	ErrUserNotFound = errors.New("user not found")

	// ErrUserExists is returned when a user exists and there is a conflict
	ErrUserExists = errors.New("user with that username exists")

	// ErrUserDisabled is returned when a user is disabled
	ErrUserDisabled = errors.New("user disabled")

	// ErrPasswordExpired is returned when a user's password is expired
	ErrPasswordExpired = errors.New("password expired")

	// ErrObjectStoreNotFound is returned when an object store does not exist
	ErrObjectStoreNotFound = errors.New("store not found")

	// ErrObjectNotFound is returned when a object does not exist
	ErrObjectNotFound = errors.New("object not found")

	// ErrNotImplemented is returns when an interface method is not implemented or supported
	ErrNotImplemented = errors.New("not implemented")
)
