module gitlab.com/ModelRocket/sparks

go 1.13

replace github.com/go-swagger/go-swagger => github.com/ModelRocket/go-swagger v0.18.2

replace github.com/go-openapi/swag => github.com/ModelRocket/swag v0.18.1-0.20190131001703-fac89298f321

replace github.com/jinzhu/gorm => github.com/ModelRocket/gorm v1.9.3-0.20191023003539-2b87c255101a

replace gitlab.com/ModelRocket/sparks/keychain => ./keychain

replace gitlab.com/ModelRocket/sparks/providers => ./providers

require (
	github.com/aws/aws-lambda-go v1.13.3 // indirect
	github.com/aws/aws-sdk-go v1.29.23
	github.com/denisenkom/go-mssqldb v0.0.0-20191128021309-1d7a30a10f73 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/erikstmartin/go-testdb v0.0.0-20160219214506-8d10e4a1bae5 // indirect
	github.com/go-openapi/strfmt v0.19.4
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/gorilla/context v1.1.1 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.1 // indirect
	github.com/jmespath/go-jmespath v0.3.0 // indirect
	github.com/kr/pretty v0.1.0
	github.com/mattn/go-sqlite3 v2.0.2+incompatible // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/spf13/cast v1.3.1
	github.com/urfave/negroni v1.0.0
	gitlab.com/ModelRocket/mu v0.0.0-20190919225050-c90a5928233b
	gitlab.com/ModelRocket/reno v1.0.1
	go.mongodb.org/mongo-driver v1.2.0 // indirect
	google.golang.org/appengine v1.6.5 // indirect
)
