/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

// Package keychain manages the validation and processing of jwt/oauth tokens
package keychain

import (
	"context"
	"crypto/rsa"
	"crypto/tls"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math/big"
	"net/http"
	"os"
	"strings"
	"sync"

	"github.com/urfave/negroni"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/ModelRocket/reno/types"
)

type (
	keyChain struct {
		Keys []struct {
			Alg string `json:"alg"`
			E   string `json:"e"`
			KID string `json:"kid"`
			KTY string `json:"kty"`
			N   string `json:"n"`
			Use string `json:"use"`
		} `json:"keys"`
	}

	// ValidationRule is a token validation rule
	ValidationRule struct {
		// Scopes is the list of scopes to match
		Scopes []string

		// Match is the rule to match the scopes with
		Match ScopeMatch

		// Issuers matches the particular issuers
		Issuers []string

		// Action is the action to perform
		Action ValidationAction

		// ClientID will match the client id
		ClientID *string

		// HMACSecret is a secret to use for validation
		HMACSecret []byte
	}

	// ScopeMatch defines a scope matching rule
	ScopeMatch string

	// ValidationAction is the action the explicit validation should take
	ValidationAction string
)

var (
	publicKeys = make(map[string]*rsa.PublicKey)

	authorizedKeys = make([]string, 0)

	// ErrInvalidKey is returned when a public key is invalid
	ErrInvalidKey = errors.New("invalid public key")

	keyLock sync.Mutex

	// ScopeMatchAny will approve/deny on any scope
	ScopeMatchAny ScopeMatch = "any"

	// ScopeMatchAll will approve/deny only if all scopes match
	ScopeMatchAll ScopeMatch = "all"

	// ValidationActionAllow explicity allow on rule match
	ValidationActionAllow ValidationAction = "allow"

	// ValidationActionDeny explicitly deny on rule match
	ValidationActionDeny ValidationAction = "deny"
)

func init() {
	// import the oauth keys into the keychain
	authKeys := types.ParseStringParams(os.Getenv("OAUTH_PUBLIC_KEYS"))

	// import the auth public keys
	for _, u := range authKeys.StringSlice("url") {
		ImportKeysFromURL(u)
	}
}

// ValidateToken validates a jwt token
func ValidateToken(tokenString string, rules ...ValidationRule) (AuthToken, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		switch token.Method.(type) {
		case *jwt.SigningMethodHMAC:
			for _, r := range rules {
				if r.HMACSecret != nil {
					return r.HMACSecret, nil
				}
			}
			return nil, ErrInvalidKey
		case *jwt.SigningMethodRSA:
		default:
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		kid, _ := token.Header["kid"].(string)
		if pubKey, ok := publicKeys[kid]; ok {
			return pubKey, nil
		}

		if !types.Slice(authorizedKeys).Contains(kid) {
			return nil, types.ErrNotAuthorized.Reason(ErrInvalidToken)
		}

		_, ok := token.Claims.(jwt.MapClaims)
		if !ok {
			return nil, types.ErrNotAuthorized.Reason(ErrInvalidToken)
		}

		if pubKey, ok := publicKeys[kid]; ok {
			return pubKey, nil
		}

		return nil, ErrInvalidKey
	})

	if err != nil || !token.Valid {
		if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Inner != ErrInvalidKey {
				return nil, types.ErrNotAuthorized.Reason(TokenValidationError(err))
			}
		}
	}

	rval := &authToken{
		token:  tokenString,
		claims: token.Claims.(jwt.MapClaims),
	}

	action := ValidationActionDeny

	for _, r := range rules {
		actionSet := false

		if len(r.Issuers) > 0 && !types.Slice(r.Issuers).ContainsAny(rval.Issuer()) {
			continue
		}

		if r.ClientID != nil && *r.ClientID != rval.ClientID() {
			continue
		}

		if len(r.Scopes) > 0 {
			tmp := make([]interface{}, 0)
			for _, v := range r.Scopes {
				tmp = append(tmp, v)
			}

			if r.Match == ScopeMatchAny {
				if types.Slice(rval.Scope()).ContainsAny(tmp...) {
					action = r.Action
					actionSet = true
					break
				}
			} else {
				if types.Slice(rval.Scope()).Contains(tmp...) {
					action = r.Action
					actionSet = true
					break
				}
			}
		}

		if actionSet {
			break
		}
	}

	if action != ValidationActionAllow {
		return nil, types.ErrNotAuthorized.Reason(ErrAccessDenied)
	}

	return rval, nil
}

// ImportKeysFromURL will fetch and import the public keys from the specified url
func ImportKeysFromURL(jwksURL string) error {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}

	resp, err := client.Get(jwksURL)
	if err != nil {
		return err
	}
	if resp.StatusCode != http.StatusOK {
		return ErrInvalidKey
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	if err := ImportKeys(data); err != nil {
		return err
	}

	return nil
}

// ImportKeys import keys in the standard jwks json format
func ImportKeys(keyData []byte) error {
	keys := &keyChain{}
	if err := json.Unmarshal(keyData, &keys); err != nil {
		return err
	}

	if len(keys.Keys) < 1 {
		return errors.New("invalid jwks keys")
	}

	for _, key := range keys.Keys {
		rawN := key.N
		rawE := key.E

		decodedE, err := base64.RawURLEncoding.DecodeString(rawE)
		if err != nil {
			return err
		}
		if len(decodedE) < 4 {
			ndata := make([]byte, 4)
			copy(ndata[4-len(decodedE):], decodedE)
			decodedE = ndata
		}
		pubKey := &rsa.PublicKey{
			N: &big.Int{},
			E: int(binary.BigEndian.Uint32(decodedE[:])),
		}
		decodedN, err := base64.RawURLEncoding.DecodeString(rawN)
		if err != nil {
			return err
		}
		pubKey.N.SetBytes(decodedN)

		publicKeys[key.KID] = pubKey

		authorizedKeys = append(authorizedKeys, key.KID)
	}

	return nil
}

// ValidateMiddleware parses an http request and validate the bearer token and puts
// it in the request context
func ValidateMiddleware(requireAuth bool) negroni.Handler {
	return negroni.HandlerFunc(func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		var bearer string

		// check for an auth token in the headers or query
		if auth := r.Header.Get("Authorization"); auth != "" {
			parts := strings.Fields(auth)
			if len(parts) < 2 || parts[0] != "Bearer" {
				http.Error(w, "access denied", http.StatusForbidden)
				return
			}

			bearer = parts[1]
		} else {
			bearer = r.URL.Query().Get("access_token")
		}

		if bearer != "" {
			authToken, err := ValidateToken(bearer)
			if err != nil {
				http.Error(w, err.Error(), http.StatusForbidden)
				return
			}

			r = r.WithContext(authToken.Context(context.TODO()))
		} else if requireAuth {
			http.Error(w, "access denied", http.StatusForbidden)
			return
		}

		next.ServeHTTP(w, r)
	})
}

// AuthTokenFromContext returns the cloud authtoken from the context
func AuthTokenFromContext(c context.Context) AuthToken {
	token, _ := c.Value(ContextKeyAuthToken).(AuthToken)
	return token
}
