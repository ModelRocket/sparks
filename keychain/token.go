/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package keychain

import (
	"context"
	"strings"

	"gitlab.com/ModelRocket/reno/types"
)

type (

	// Claims is an alias from Params (types.StringMap)
	Claims = types.Params

	// AuthToken is a driver interface for parsing and using JWT values
	AuthToken interface {
		// ID returns the token identifier
		ID() string

		// ClientID returns the OAuth client identity
		ClientID() string

		// Username returns the user for the token or empty if no user is associated
		Username() string

		// Subject return the subject id
		Subject() string

		// Audience return the token audience
		Audience() string

		// ExpiresAt returns the token expiration time
		ExpiresAt() int64

		// Scope returns the scopes the token has
		Scope() []string

		// Returns the token use, i.e. access, identity, etc.
		Use() string

		// Claims returns the token claims
		Claims() Claims

		// String returns the string value of the token as a signed JWT
		String() string

		// Returns a context from the token
		Context(context.Context) context.Context
	}

	// authToken is an JWT authToken
	authToken struct {
		token  string
		claims map[string]interface{}
	}
)

// EmptyToken returns a basic empty client token
func EmptyToken(clientID string, claims map[string]interface{}) AuthToken {
	token := &authToken{
		claims: claims,
	}

	if token.claims == nil {
		token.claims = make(map[string]interface{})
	}

	token.claims["client_id"] = clientID

	return token
}

// ID returns the token id
func (t *authToken) ID() string {
	if jti, ok := t.claims["jti"]; ok {
		return jti.(string)
	}
	return ""
}

// ClientID returns the token client id
func (t *authToken) ClientID() string {
	if clientID, ok := t.claims["client_id"]; ok {
		return clientID.(string)
	}
	return ""
}

// Username returns the user for the tokem
func (t *authToken) Username() string {
	if username, ok := t.claims["username"]; ok {
		return username.(string)
	}

	return ""
}

// Subject returns the subject for the tokem
func (t *authToken) Subject() string {
	if sub, ok := t.claims["sub"]; ok {
		return sub.(string)
	}

	return ""
}

// Audience returns the audience for the tokem
func (t *authToken) Audience() string {
	if sub, ok := t.claims["aud"]; ok {
		return sub.(string)
	}

	return ""
}

// String returns the string value of the token
func (t *authToken) String() string {
	return t.token
}

// Claims returns the underlying claims
func (t *authToken) Claims() Claims {
	return t.claims
}

// Scope returns the token scope
func (t *authToken) Scope() []string {
	if scope := t.Claims().String("scope"); scope != "" {
		return strings.Fields(scope)
	} else if scope := t.Claims().StringSlice("scp"); len(scope) > 0 {
		return scope
	}
	return []string{}
}

// Use returns the token usage
func (t *authToken) Use() string {
	if scope, ok := t.claims["token_use"]; ok {
		return scope.(string)
	}
	return ""
}

// Issuer returns the token issuer
func (t *authToken) Issuer() string {
	if scope, ok := t.claims["iss"]; ok {
		return scope.(string)
	}
	return ""
}

// Context implements the authToken context wrapper
func (t *authToken) Context(c context.Context) context.Context {
	return context.WithValue(c, ContextKeyAuthToken, t)
}

// ExpiresAt returns the epoch for the token expiration
func (t *authToken) ExpiresAt() int64 {
	if exp, ok := t.claims["exp"]; ok {
		return int64(exp.(float64))
	}
	return 0
}
