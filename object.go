/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package sparks

import (
	"io"
	"os"
	"time"

	"gitlab.com/ModelRocket/reno/types"
)

type (
	// ObjectProvider provides an object store
	ObjectProvider interface {
		// OpenObjectStore store returns an object store with the given name and parameter
		OpenObjectStore(name string, params ...Params) (ObjectStore, error)
	}

	// ObjectStore is an interface for kv object stores implemented by providers
	ObjectStore interface {
		// GetObject returns an io.Reader that allows for getting the object data
		GetObject(key string, params ...Params) (io.Reader, error)

		// GetObjectLink returns a secure http link to the object
		GetObjectLink(key string, timeout time.Duration, write ...bool) (*types.Link, error)

		// PutObject set the value
		PutObject(key string, r io.Reader, params ...Params) error

		// DeleteObject deletes and object
		DeleteObject(key string, params ...Params) error

		// StatObject returns properties of the object
		StatObject(key string) (types.StringMap, error)

		// ListObjects returns a list of object keys for the specified prefix
		ListObjects(prefix string) ([]os.FileInfo, error)

		// Name returns the object store name
		Name() string
	}
)
