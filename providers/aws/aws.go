/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package aws

import (
	"os"
	"sync"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	sparks "gitlab.com/ModelRocket/sparks"
)

type (
	// Provider is an AWS cloud provider
	Provider struct {
		session *session.Session
		params  sparks.Params
	}
)

var (
	once sync.Once

	defaultProvider *Provider
)

const (
	// Name is the provider name
	Name = "aws"
)

func init() {
	sparks.RegisterProvider("aws", New)
}

// DefaultProvider returns the default aws provider
func DefaultProvider() *Provider {
	once.Do(func() {
		if p, err := New(sparks.Params{}); err == nil {
			defaultProvider = p.(*Provider)
		}
	})

	return defaultProvider
}

// New returns a new AWS provider
func New(params sparks.Params) (sparks.Provider, error) {
	var creds *credentials.Credentials

	inst := &Provider{
		params: params,
	}

	// if we are in a lambda we use the local credentials
	if _, ok := os.LookupEnv("LAMBDA_TASK_ROOT"); !ok {
		if _, ok := os.LookupEnv("AWS_CONTAINER_CREDENTIALS_RELATIVE_URI"); !ok {
			creds = credentials.NewSharedCredentials("", inst.params.String("profile"))
		}
	}

	defaultRegion := os.Getenv("AWS_REGION")
	if defaultRegion == "" {
		defaultRegion = "us-east-1"
	}

	// Setup the aws globals
	session, err := session.NewSession(&aws.Config{
		Region:      aws.String(inst.params.String("region", defaultRegion)),
		Credentials: creds,
	})
	if err != nil {
		return nil, err
	}

	inst.session = session

	return inst, nil
}

// Name returns the provider name
func (p *Provider) Name() string {
	return Name
}

// Close closes and cleans up the provider
func (p *Provider) Close() {

}
