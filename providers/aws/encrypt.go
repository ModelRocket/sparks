/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package aws

import (
	"github.com/aws/aws-sdk-go/service/kms"
	"gitlab.com/ModelRocket/sparks"
)

// Decrypt implements the cloud.Encryption provider interface using KMS
func (p *Provider) Decrypt(data []byte, params sparks.Params) ([]byte, error) {

	kmsProv := kms.New(p.session)

	out, err := kmsProv.Decrypt(&kms.DecryptInput{
		CiphertextBlob: data,
	})
	if err != nil {
		return nil, err
	}
	return out.Plaintext, nil
}

// Encrypt implements the cloud.Encryption provider interface using KMS
func (p *Provider) Encrypt(data []byte, params sparks.Params) ([]byte, error) {

	kmsProv := kms.New(p.session)

	out, err := kmsProv.Encrypt(&kms.EncryptInput{
		Plaintext: data,
		KeyId:     params.StringPtr("keyId"),
	})
	if err != nil {
		return nil, err
	}
	return out.CiphertextBlob, nil
}
