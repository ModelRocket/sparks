/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package aws

import (
	"bytes"
	"io"
	"os"
	"path"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/go-openapi/strfmt"
	"gitlab.com/ModelRocket/reno/types"
	"gitlab.com/ModelRocket/reno/util"
	"gitlab.com/ModelRocket/sparks"
)

type (
	objectStore struct {
		name       string
		params     sparks.Params
		session    *session.Session
		downloader *s3manager.Downloader
	}

	listObject struct {
		name         string
		size         int64
		lastModified time.Time
	}

	dummyWriter struct {
		w io.Writer
	}
)

// OpenObjectStore returns an instance to an S3 bucket
func (p *Provider) OpenObjectStore(name string, params ...sparks.Params) (sparks.ObjectStore, error) {
	var pp sparks.Params

	if len(params) > 0 {
		pp = params[0]
	}

	downloader := s3manager.NewDownloader(p.session)
	downloader.Concurrency = 1

	return &objectStore{
		name:       name,
		params:     pp,
		session:    p.session,
		downloader: downloader,
	}, nil
}

func (o *objectStore) Name() string {
	return o.name
}

// GetObject returns an object from the object store
func (o *objectStore) GetObject(key string, params ...sparks.Params) (io.Reader, error) {
	data := new(bytes.Buffer)

	_, err := o.downloader.Download(dummyWriter{data}, &s3.GetObjectInput{
		Bucket: aws.String(o.name),
		Key:    aws.String(key),
	})
	if err != nil {
		if awserr, ok := err.(awserr.Error); ok {
			if awserr.Code() == s3.ErrCodeNoSuchKey {
				return nil, sparks.ErrObjectNotFound
			} else if awserr.Code() == s3.ErrCodeNoSuchBucket {
				return nil, sparks.ErrObjectStoreNotFound
			}
		}
		return nil, err
	}

	return bytes.NewReader(data.Bytes()), nil
}

func (o *objectStore) GetObjectLink(key string, timeout time.Duration, write ...bool) (*types.Link, error) {
	svc := s3.New(o.session)

	if timeout < time.Minute*15 {
		timeout = time.Minute * 15
	}

	expires := time.Now().Add(timeout)

	if len(write) < 0 || !write[0] {
		req, _ := svc.GetObjectRequest(&s3.GetObjectInput{
			Bucket: aws.String(o.name),
			Key:    aws.String(key),
		})
		str, err := req.Presign(timeout)
		if err != nil {
			return nil, err
		}
		return &types.Link{
			Href:      strfmt.URI(str),
			ExpiresAt: expires.Unix(),
		}, nil
	}

	req, _ := svc.PutObjectRequest(&s3.PutObjectInput{
		Bucket: aws.String(o.name),
		Key:    aws.String(key),
	})
	str, err := req.Presign(timeout)
	if err != nil {
		return nil, err
	}

	return &types.Link{
		Href:      strfmt.URI(str),
		ExpiresAt: expires.Unix(),
	}, nil
}

// PutObject puts an object into the object store
func (o *objectStore) PutObject(key string, r io.Reader, params ...sparks.Params) error {
	p := sparks.Params{}

	if len(params) > 0 {
		p = params[0]
	}

	input := &s3manager.UploadInput{
		Bucket:             aws.String(o.name),
		Key:                aws.String(key),
		Body:               r,
		ContentDisposition: p.StringPtr("content-disposition"),
		ContentType:        p.StringPtr("content-type"),
		ContentEncoding:    p.StringPtr("content-encoding"),
		Tagging:            p.StringPtr("tags"),
	}

	uploader := s3manager.NewUploader(o.session)
	_, err := uploader.Upload(input)
	if err != nil {
		return err
	}

	return nil
}

func (o *objectStore) StatObject(key string) (types.StringMap, error) {
	svc := s3.New(o.session)

	out, err := svc.HeadObject(&s3.HeadObjectInput{
		Bucket: aws.String(o.name),
		Key:    aws.String(key),
	})

	if err != nil {
		if awserr, ok := err.(awserr.Error); ok {
			if awserr.Code() == s3.ErrCodeNoSuchKey {
				return nil, sparks.ErrObjectNotFound
			} else if awserr.Code() == s3.ErrCodeNoSuchBucket {
				return nil, sparks.ErrObjectStoreNotFound
			}
		}
		return nil, err
	}

	rval := util.StructMap(out)

	return types.StringMap(rval), nil
}

// ListObjects lists objects for the specified prefix
func (o *objectStore) ListObjects(prefix string) ([]os.FileInfo, error) {
	svc := s3.New(o.session)

	out, err := svc.ListObjectsV2(&s3.ListObjectsV2Input{
		Bucket: aws.String(o.name),
		Prefix: aws.String(prefix),
	})
	if err != nil {
		return nil, err
	}

	rval := make([]os.FileInfo, 0)
	for _, o := range out.Contents {
		rval = append(rval, &listObject{
			name:         *o.Key,
			size:         *o.Size,
			lastModified: *o.LastModified,
		})
	}

	return rval, nil
}

// DeleteObject deletes and object
func (o *objectStore) DeleteObject(key string, params ...sparks.Params) error {
	s := s3.New(o.session)

	if len(params) > 0 {
		if params[0].Bool("removeAll") {
			out, err := s.ListObjectsV2(&s3.ListObjectsV2Input{
				Bucket: aws.String(o.name),
				Prefix: aws.String(path.Join(key, "/")),
			})
			if err != nil {
				return err
			}

			for _, obj := range out.Contents {
				if *obj.Key == key {
					continue
				}
				if err := o.DeleteObject(*obj.Key, params[0]); err != nil {
					return err
				}
			}
		}
	}

	if _, err := s.DeleteObject(&s3.DeleteObjectInput{
		Bucket: aws.String(o.name),
		Key:    aws.String(key),
	}); err != nil {
		return err
	}

	return nil
}

func (l *listObject) Name() string {
	return l.name
}

func (l *listObject) Size() int64 {
	return l.size
}

func (l *listObject) Mode() os.FileMode {
	return 0
}

func (l *listObject) ModTime() time.Time {
	return l.lastModified
}

func (l *listObject) IsDir() bool {
	return false
}

func (l *listObject) Sys() interface{} {
	return nil
}

func (d dummyWriter) WriteAt(p []byte, offset int64) (n int, err error) {
	// ignore 'offset' because we forced sequential downloads
	return d.w.Write(p)
}
