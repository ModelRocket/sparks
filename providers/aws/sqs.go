/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package aws

import (
	"context"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"gitlab.com/ModelRocket/reno/types"
	"gitlab.com/ModelRocket/sparks"
)

type sqsQueue struct {
	svc               *sqs.SQS
	url               string
	backlog           int64
	pollInterval      time.Duration
	visibilityTimeout int64
	waitTimeSeconds   int64
}

type sqsMessage struct {
	m *sqs.Message
}

// OpenQueue opens an SQS queue
func (p *Provider) OpenQueue(name string, params sparks.Params) (sparks.Queue, error) {
	return &sqsQueue{
		svc:               sqs.New(p.session),
		url:               name,
		backlog:           params.Int64("backlog", 16),
		pollInterval:      params.Duration("pollInterval", time.Second*5),
		visibilityTimeout: params.Int64("visibilityTimeout", 60),
		waitTimeSeconds:   params.Int64("waitTimeSeconds", 20),
	}, nil
}

// Publish implements the sparks.queue interface
func (q *sqsQueue) Publish(body []byte, attributes ...sparks.Attributes) (string, error) {
	result, err := q.svc.SendMessage(&sqs.SendMessageInput{
		DelaySeconds: aws.Int64(0),
		MessageBody:  aws.String(string(body)),
		QueueUrl:     &q.url,
	})
	if err != nil {
		return "", err
	}
	return *result.MessageId, nil
}

// Scan implements the sparks.Queue interface
func (q *sqsQueue) Scan(ctx context.Context, handler func(msg sparks.QueueMessage) error) error {
	timer := time.NewTimer(time.Second * 1)

	for {
		select {
		case <-timer.C:
			result, err := q.svc.ReceiveMessage(&sqs.ReceiveMessageInput{
				MessageAttributeNames: []*string{
					aws.String(sqs.QueueAttributeNameAll),
				},
				QueueUrl:            &q.url,
				MaxNumberOfMessages: aws.Int64(q.backlog),
				VisibilityTimeout:   aws.Int64(q.visibilityTimeout),
				WaitTimeSeconds:     aws.Int64(q.waitTimeSeconds),
			})
			if err != nil {
				return err
			}

			for _, m := range result.Messages {
				if err := handler(&sqsMessage{m}); err != nil {
					return err
				}
			}

			if len(result.Messages) == 0 {
				timer = time.NewTimer(q.pollInterval)
			} else {
				timer = time.NewTimer(time.Second * 1)
			}

		case <-ctx.Done():
			return nil
		}
	}

}

// MessageID implements the sparks.QueueMessage interface
func (m *sqsMessage) MessageID() string {
	if m.m.MessageId == nil {
		return ""
	}
	return *m.m.MessageId
}

func (m sqsMessage) Body() []byte {
	if m.m.Body == nil {
		return make([]byte, 0)
	}
	return []byte(*m.m.Body)
}

// Attributes implements the sparks.QueueMessage interface
func (m *sqsMessage) Attributes() types.StringMap {
	return nil
}
