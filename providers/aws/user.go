/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package aws

import (
	"errors"
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
	cache "github.com/patrickmn/go-cache"
	"github.com/spf13/cast"
	"gitlab.com/ModelRocket/reno/types"
	"gitlab.com/ModelRocket/sparks"
	"gitlab.com/ModelRocket/sparks/keychain"
)

type (
	// UserPool is a congnito UserPool
	UserPool struct {
		cognito  *cognitoidentityprovider.CognitoIdentityProvider
		pool     string
		clientID string
		params   sparks.Params
	}

	// User is a cognito User
	User struct {
		*cognitoidentityprovider.UserType
		pool   *UserPool
		groups []string
	}

	// AuthToken is an JWT AuthToken
	AuthToken struct {
		keychain.AuthToken
	}

	// AuthChallenge is a congnito auth challenge
	AuthChallenge struct {
		Name    *string
		Params  map[string]*string
		Session *string
	}
)

var (
	userCache = cache.New(5*time.Minute, 10*time.Minute)
)

const (
	// JWKSURL is the cognito well-known public keys url
	JWKSURL = "https://cognito-idp.%s.amazonaws.com/%s/.well-known/jwks.json"
)

// OpenUserPool returns a cognito user pool instance.
// This method takes the following required parameters:
// 		client_id: the cognito client id
//		public_keys: base64 encoded json string, retrieved from:
// 			https://cognito-idp.<region>.amazonaws.com/<pool_id>/.well-known/jwks.json
func (p *Provider) OpenUserPool(name string, options sparks.Params) (sparks.UserPool, error) {
	clientID := options.String("client_id")

	jwksURL := fmt.Sprintf(JWKSURL, p.params.String("region", "us-east-1"), name)
	if err := keychain.ImportKeysFromURL(jwksURL); err != nil {
		return nil, err
	}

	pool := &UserPool{
		cognito:  cognitoidentityprovider.New(p.session),
		pool:     name,
		clientID: clientID,
		params:   options,
	}

	return pool, nil
}

// AuthenticateUser authenticates the user and returns an AuthToken
func (p *UserPool) AuthenticateUser(username, password string) (keychain.AuthToken, error) {
	var accessToken string

	resp, err := p.cognito.AdminInitiateAuth(&cognitoidentityprovider.AdminInitiateAuthInput{
		UserPoolId: aws.String(p.pool),
		ClientId:   aws.String(p.clientID),
		AuthFlow:   aws.String("ADMIN_NO_SRP_AUTH"),
		AuthParameters: map[string]*string{
			"USERNAME": aws.String(username),
			"PASSWORD": aws.String(password),
		},
	})
	if err != nil {
		return nil, err
	}
	if resp.ChallengeName != nil {
		return nil, &AuthChallenge{
			Name:    resp.ChallengeName,
			Params:  resp.ChallengeParameters,
			Session: resp.Session,
		}
	}

	if resp.AuthenticationResult != nil && resp.AuthenticationResult.AccessToken != nil {
		accessToken = *resp.AuthenticationResult.AccessToken
	}

	if accessToken == "" {
		return nil, keychain.ErrInvalidToken
	}

	return keychain.ValidateToken(accessToken)
}

// CreateUser creates a new user
func (p *UserPool) CreateUser(username, password string, attributes map[string]string, options ...sparks.Params) (sparks.User, error) {
	resend := false
	for _, o := range options {
		if o.Bool("ResendInvite") {
			resend = true
		}
	}

	attrs := make([]*cognitoidentityprovider.AttributeType, 0)

	for k, v := range attributes {
		attrs = append(attrs, &cognitoidentityprovider.AttributeType{
			Name:  aws.String(k),
			Value: aws.String(v),
		})
	}

	var tempPassword *string

	if password != "" {
		tempPassword = aws.String(password)
	}

	input := &cognitoidentityprovider.AdminCreateUserInput{
		UserPoolId:         aws.String(p.pool),
		Username:           aws.String(username),
		TemporaryPassword:  tempPassword,
		UserAttributes:     attrs,
		ForceAliasCreation: aws.Bool(p.params.Bool("force_alias", false)),
	}

	out, err := p.cognito.AdminCreateUser(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			if aerr.Code() == cognitoidentityprovider.ErrCodeUsernameExistsException && resend {
				input.MessageAction = aws.String("RESEND")
				input.UserAttributes = make([]*cognitoidentityprovider.AttributeType, 0)
				input.TemporaryPassword = nil
			}
			out, err = p.cognito.AdminCreateUser(input)
			if err != nil {
				return nil, err
			}
		} else {
			return nil, err
		}
	}

	return &User{
		UserType: out.User,
	}, nil
}

// GetUser returns a user
func (p *UserPool) GetUser(username string) (sparks.User, error) {
	if user, ok := userCache.Get(username); ok {
		return user.(sparks.User), nil
	}

	// Get the user data from cognito
	resp, err := p.cognito.AdminGetUser(&cognitoidentityprovider.AdminGetUserInput{
		UserPoolId: aws.String(p.pool),
		Username:   aws.String(username),
	})
	if err != nil {
		return nil, mapCognitoError(err)
	}

	// Get the user groups from cognito
	groupResp, err := p.cognito.AdminListGroupsForUser(&cognitoidentityprovider.AdminListGroupsForUserInput{
		UserPoolId: aws.String(p.pool),
		Username:   aws.String(username),
	})
	if err != nil {
		return nil, mapCognitoError(err)
	}
	groups := make([]string, 0)
	for _, g := range groupResp.Groups {
		groups = append(groups, *g.GroupName)
	}

	user := &User{
		UserType: &cognitoidentityprovider.UserType{
			Username:   resp.Username,
			Enabled:    resp.Enabled,
			Attributes: resp.UserAttributes,
			UserStatus: resp.UserStatus,
		},
		pool:   p,
		groups: groups,
	}

	userCache.Add(username, user, cache.DefaultExpiration)

	return user, nil
}

// ChangeUserPassword changes the users password
// This method first attempt to authenticate the user to get a session token
// then it uses this token to perform the actual password change.
func (p *UserPool) ChangeUserPassword(username, current, proposed string) error {
	// we need to get an access token to change the users password
	token, err := p.AuthenticateUser(username, current)
	if err != nil {
		// the response was an auth challenge, so this is a force password change
		challenge, ok := err.(*AuthChallenge)
		if !ok {
			return err
		}

		if *challenge.Name != "NEW_PASSWORD_REQUIRED" {
			return errors.New("Unsupported authentication challenge")
		}

		_, err := p.cognito.AdminRespondToAuthChallenge(&cognitoidentityprovider.AdminRespondToAuthChallengeInput{
			UserPoolId:    aws.String(p.pool),
			ClientId:      aws.String(p.clientID),
			ChallengeName: challenge.Name,
			ChallengeResponses: map[string]*string{
				"USERNAME":     aws.String(username),
				"NEW_PASSWORD": aws.String(proposed),
			},
			Session: challenge.Session,
		})

		return err
	}

	if _, err := p.cognito.ChangePassword(&cognitoidentityprovider.ChangePasswordInput{
		AccessToken:      aws.String(token.String()),
		PreviousPassword: aws.String(current),
		ProposedPassword: aws.String(proposed),
	}); err != nil {
		return err
	}

	return nil
}

// UpdateUser updates a users attributes
func (p *UserPool) UpdateUser(username string, attributes types.StringMap) error {
	attrs := make([]*cognitoidentityprovider.AttributeType, 0)

	for k, v := range attributes {
		attrs = append(attrs, &cognitoidentityprovider.AttributeType{
			Name:  aws.String(k),
			Value: aws.String(cast.ToString(v)),
		})
	}

	_, err := p.cognito.AdminUpdateUserAttributes(&cognitoidentityprovider.AdminUpdateUserAttributesInput{
		UserPoolId:     aws.String(p.pool),
		Username:       aws.String(username),
		UserAttributes: attrs,
	})
	if err == nil {
		userCache.Delete(username)
	}

	return mapCognitoError(err)
}

// DeleteUser deletes a user record
func (p *UserPool) DeleteUser(username string) error {
	_, err := p.cognito.AdminDeleteUser(&cognitoidentityprovider.AdminDeleteUserInput{
		UserPoolId: aws.String(p.pool),
		Username:   aws.String(username),
	})

	return mapCognitoError(err)
}

// DisableUser disables a user account
func (p *UserPool) DisableUser(username string) error {
	_, err := p.cognito.AdminDisableUser(&cognitoidentityprovider.AdminDisableUserInput{
		UserPoolId: aws.String(p.pool),
		Username:   aws.String(username),
	})

	return err
}

// Login returns the user login i.e. username
func (u *User) Login() string {
	return *u.Username
}

// Status returns the user status
func (u *User) Status() string {
	return *u.UserStatus
}

// Attributes returns a map of user attributes
func (u *User) Attributes() types.StringMap {
	attrs := make(types.StringMap)

	for _, attr := range u.UserType.Attributes {
		attrs[*attr.Name] = *attr.Value
	}
	return attrs
}

// Enabled represents the user's status
func (u *User) Enabled() bool {
	return *u.UserType.Enabled
}

// Groups returns a list of groups the user belongs to
func (u *User) Groups() []string {
	return u.groups
}

func (c *AuthChallenge) Error() string {
	return fmt.Sprintf("authentication returned challenge: %s", *c.Name)
}

// ID returns the token id
func (t *AuthToken) ID() string {
	if t.Use() == "id" {
		return t.Claims().String("event_id")
	}

	return t.Claims().String("jti")
}

// Username returns the token username
func (t *AuthToken) Username() string {
	switch t.Use() {
	case "id":
		return t.Claims().String("cognito:username")
	default:
		return t.Username()
	}

}

// Scope returns the token scope
func (t *AuthToken) Scope() []string {
	if t.Use() == "id" {
		return []string{"aws.cognito.signin.user.admin"}
	}
	return t.Scope()
}

// here we map errors to the appropriate generic provider errors
func mapCognitoError(err error) error {
	if err == nil {
		return err
	}
	if awserr, ok := err.(awserr.Error); ok {
		switch awserr.Code() {
		case cognitoidentityprovider.ErrCodeUserNotFoundException:
			return sparks.ErrUserNotFound
		case cognitoidentityprovider.ErrCodeResourceNotFoundException:
			return sparks.ErrUserPoolNotFound
		}
	}
	return err
}
