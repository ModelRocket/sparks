/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package sparks

import (
	"context"
	"errors"

	"gitlab.com/ModelRocket/reno/types"
)

// QueueProvider defines a queue provider interface
type QueueProvider interface {
	OpenQueue(name string, params Params) (Queue, error)
}

// QueueMessage is a queue message interface
type QueueMessage interface {
	// MessageID returns a unique message identifier
	MessageID() string

	// Body returns the message body as []byte
	Body() []byte

	// Attributes returns the message attributes as a map
	Attributes() types.StringMap
}

// QueueError is a queue error type
type QueueError error

var (
	// QueueErrorScanCancel can be returned by the scan handler to stop scanning
	QueueErrorScanCancel = QueueError(errors.New("scan canceled"))
)

// Queue defines a queue interface
type Queue interface {
	// Publish publishes the message body with optioanl attributes and return the id
	Publish(body []byte, attributes ...Attributes) (string, error)

	// Scan performs a canceleable scan on the queue
	Scan(ctx context.Context, handler func(msg QueueMessage) error) error
}
