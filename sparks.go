/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

// Package sparks provides the interfaces and manager for cloud providers
package sparks

import (
	"encoding/base64"
	"fmt"
	"os"
	"sync"

	"gitlab.com/ModelRocket/reno/types"
)

type (
	// Params is an alias for reno/types.Params
	Params = types.Params

	// Attributes is an alias for reno/types.Params
	Attributes = types.Params
)

var (
	provMu    sync.RWMutex
	providers = make(map[string]NewFunc)
)

// RegisterProvider makes a database provider available by the provided name.
// If RegisterProvider is called twice with the same name or if provider is nil,
// it panics.
func RegisterProvider(name string, provider NewFunc) {
	provMu.Lock()
	defer provMu.Unlock()
	if provider == nil {
		panic("cloud: Register provider is nil")
	}
	if _, dup := providers[name]; dup {
		panic("cloud: Register called twice for provider " + name)
	}
	providers[name] = provider
}

// OpenProvider opens a new cloud provider instance with the specified parameters
func OpenProvider(name string, params types.Params) (Provider, error) {
	if params == nil {
		params = make(types.Params)
	}
	provMu.RLock()
	createFn, ok := providers[name]
	provMu.RUnlock()
	if !ok {
		return nil, fmt.Errorf("cloud: unknown provider %q (forgotten import?)", name)
	}
	return createFn(params)
}

// DefaultProvider returns the default based on the environment provider or panics
func DefaultProvider() Provider {
	prov, err := OpenProvider(os.Getenv("CLOUD_PROVIDER"), types.ParseStringParams(os.Getenv("PROVIDER_OPTIONS")))
	if err != nil {
		panic(err)
	}

	return prov
}

// DefaultObjectStore returns the default object store based on the env or panics
func DefaultObjectStore() ObjectStore {
	store, err := DefaultProvider().(ObjectProvider).OpenObjectStore(os.Getenv("OBJECT_STORE"),
		types.ParseStringParams(os.Getenv("OBJECT_STORE_PARAMS")))
	if err != nil {
		panic(err)
	}

	return store
}

// Getenv decrypt a base64 encoded encrypted env var using the Encryption provider
func Getenv(key string, prov ...Provider) string {
	var p Provider

	if len(prov) > 0 {
		p = prov[0]
	} else {
		p = DefaultProvider()
	}

	val, _ := LookupEnv(key, p)

	return string(val)
}

// GetenvParams returns a parameters structure from the env var
func GetenvParams(key string, prov ...Provider) types.Params {
	val := Getenv(key, prov...)

	if val != "" {
		return types.ParseStringParams(val)
	}

	return types.Params{}
}

// LookupEnv decrypt a base64 encoded encrypted env var using the Encryption provider
func LookupEnv(key string, prov ...Provider) ([]byte, error) {
	var p Provider

	if len(prov) > 0 {
		p = prov[0]
	} else {
		p = DefaultProvider()
	}

	val := os.Getenv(key)

	blob, err := base64.StdEncoding.DecodeString(val)
	if err != nil {
		return []byte{}, err
	}

	d, err := p.(EncryptionProvider).Decrypt(blob, Params{})
	if err != nil {
		return []byte{}, err
	}

	return d, nil
}
