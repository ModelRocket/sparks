/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package sparks

import (
	"gitlab.com/ModelRocket/reno/types"
	"gitlab.com/ModelRocket/sparks/keychain"
)

type (
	// UserProvider is an interface for getting a UserPool from the provider
	UserProvider interface {
		// OpenUserPool returns a user pool with the given name and parameters
		OpenUserPool(name string, options Params) (UserPool, error)
	}

	// User is a common user interface
	User interface {
		// Login returns the user login i.e. username
		Login() string

		// Attributes returns a map of user attributes
		Attributes() types.StringMap

		// Status returns the user status as a string
		Status() string

		// Enabled represents the user's status
		Enabled() bool

		// Groups returns a list of groups the user belongs to
		Groups() []string
	}

	// UserPool is an interface implemented by providers that support users
	UserPool interface {
		// AuthenticateUser authenticates the user and returns an AuthToken
		AuthenticateUser(username, password string) (keychain.AuthToken, error)

		// CreateUser creates a new user
		CreateUser(username, password string, attributes map[string]string, options ...Params) (User, error)

		// ChangeUserPassword attempts to change the users password from current to the proposed
		ChangeUserPassword(username, current, proposed string) error

		// GetUser returns a user
		GetUser(username string) (User, error)

		// UpdateUser updates a users attributes
		UpdateUser(username string, attributes types.StringMap) error

		// DeleteUser deletes a user record
		DeleteUser(username string) error

		// DisableUser disables a user account
		DisableUser(username string) error
	}
)
